import { configure, mount, render, shallow  } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

// Global available Enzyme objects
global.shallow = shallow;
global.render = render;
global.mount = mount;