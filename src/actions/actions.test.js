import * as actions from './index';
import * as constants from '../constants';

describe('actions', () => {
    it('should create an action to fetch the coin data ', () => {
      const currencies = [
          {
            id: "bitcoin",
            name:"Bitcoin",
            price_btc: "1.0"
            
          }
      ]
      const getAction = {
        type: constants.LOAD_DATA_SUCCESS,
        payload: {
            currencies
        }
      }
      expect(actions.getDataSuccess(currencies)).toEqual(getAction)
    })
  })
  