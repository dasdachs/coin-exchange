import { 
    LOADING_FINISHED, 
    LOADING_STARTED, 
    LOAD_DATA_SUCCESS, 
    LOAD_DATA_FAIL, 
    SET_CURRENCY 
} from '../constants';

import coinmarketcapAPI from '../api';

export const startLoading = () => {
    const action = {
        type: LOADING_STARTED,
    }
    return action;
}

export const dataLoaded = () => {
    const action = {
        type: LOADING_FINISHED,
    }
    return action;
}

export const getDataSuccess = (currencies) => {
    const action = {
        type: LOAD_DATA_SUCCESS,
        payload: {
            currencies
        }
    }
    return action;
}

const getDataFail = () => {
    const action = {
        type: LOAD_DATA_FAIL
    }
    return action;
}

export const changeCurrencySuccess = (currencies, userCurrency) => {
    const action = {
        type: SET_CURRENCY,
        payload: {
            currencies,
            userCurrency
        }
    }
    return action;
}

/**
 * setCurrency changes the selected fiat currency and updated the currencis in the store
 * to display the accordin prices.
 * 
 * @param {string} userCurrency 
 */
export const setCurrency = (userCurrency) => {
    return dispatch => {
        coinmarketcapAPI(userCurrency)
        .then(response => {
            dispatch(changeCurrencySuccess(response.data, userCurrency))
        })
        .catch(error => {
            console.log(error);
            getDataFail()
        })
    }
}

export const getData = (fiat="USD") => {
    return dispatch => {
        coinmarketcapAPI(fiat)
        .then(response => {
            return dispatch(getDataSuccess(response.data));
        })
        .catch(error => {
            console.log(error);
            getDataFail()
        })
    }
}