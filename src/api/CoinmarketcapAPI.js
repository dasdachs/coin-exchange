import axios from 'axios';

/**
 * apiCoinmarketcap() get's the top 100 cryptocurrencies in a given fiat currency
 * 
 * @param {string} - The name of the fiat currency (USD, EUR or CNY) 
 * @returns {promise} - Axios returns a promise with the data
 */
const coinmarketcapAPI = (fiat="USD") => {
    const url = `https://api.coinmarketcap.com/v1/ticker/?convert=${fiat}`
    return axios.get(url)
}

export default coinmarketcapAPI;