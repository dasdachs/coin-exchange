import { 
  LOADING_FINISHED, 
  LOADING_STARTED, 
  LOAD_DATA_SUCCESS, 
  LOAD_DATA_FAIL, 
  SET_CURRENCY 
}  from '../constants';

const initState = {userCurrency:'USD', currencies:[], loading:true, error:false};

const exchange = (state=initState, action) => {
    switch(action.type) {
        case LOADING_STARTED:
          return Object.assign({}, state, {loading: true});
        case LOADING_FINISHED:
          return Object.assign({}, state, {loading: false});
        case LOAD_DATA_SUCCESS:
          return Object.assign({}, state, {currencies: action.payload.currencies, loading:false});
        case LOAD_DATA_FAIL:
          // TODO: handle errors in production
          return Object.assign({}, state, {loading: false, error: true});
        case SET_CURRENCY:
          return Object.assign({}, state, 
            {userCurrency:action.payload.userCurrency, currencies:action.payload.currencies, loading:false});
        default:
          return state;
    }
}

export default exchange;