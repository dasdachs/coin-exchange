import { getJSONAttr } from "./getJSONAttr";


test('returns the JSON attribute string depending on the user\'s curency', () => {
    const userCurrency = 'EUR';
    const attribute = 'price';
    const JSONAttribue = getJSONAttr(attribute, userCurrency);
    expect(JSONAttribue).toBe('price_eur');
})

test('returns the JSON attribute string depending on the user\'s curency', () => {
    const userCurrency = 'CYN';
    const attribute = '24_volume';
    const JSONAttribue = getJSONAttr(attribute, userCurrency);
    expect(JSONAttribue).toBe('24_volume_cyn');
})