/**
 * getJSONAttr() returns the name of the JSON property
 * 
 * The price, 24 volume etc. in the object is prefixed with a string like "price_" + the currency.
 * getPriceAttr() is a helper function to set the JSON props string.
 * 
 * @param {string} attribute - Part of the name of the JSON attribute
 * @param {string} userCurrency - the chosen fiat currency (USD, EUR, CYN)
 * @returns {string} - The property string
 */
export const getJSONAttr = (attribute, userCurrency) => {
    return attribute + '_' + userCurrency.toLowerCase();
}