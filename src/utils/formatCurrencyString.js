/**
 * formatCurrencyString() makes a price representation more readable 
 * and formats it to the local representation.
 * 
 * @param {string} priceString - An unformated string holding the price
 * @param {string} userCurrency - A string representing the selected fiat currency to convert the value
 * @param {string} locale -  A string holding a BCP 47 language tag (more on https://tools.ietf.org/html/rfc5646)
 */
export const formatCurrencyString = (priceString, userCurrency, locale='si-SL') => {
    return new Intl.NumberFormat(locale, { style: 'currency', currency: userCurrency }).format(priceString);
}