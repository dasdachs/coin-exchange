/**
 * NOTE: This test preforms basic test on the formatCurrencyString. 
 * The output is excpected values are not good local representations,
 * but the real localicastion is done in the broswers, to the tests
 * are somewhat limited. 
 */
import { formatCurrencyString } from './formatCurrencyString';

test('returns the local representation of a price string', () => {
    const userCurrency = 'EUR';
    const priceString = '161232131.236346';
    const formated = formatCurrencyString(priceString, userCurrency);
    expect(formated).toBe('€161,232,131.24');
})

test('returns the local representation of a price string', () => {
    const userCurrency = 'USD';
    const priceString = '17114.6';
    const formated = formatCurrencyString(priceString, userCurrency);
    expect(formated).toMatch('US$17,114.60');
})

test('returns the local representation of a price string', () => {
    const userCurrency = 'CYN';
    const priceString = '18743600000.0';
    const formated = formatCurrencyString(priceString, userCurrency);
    expect(formated).toMatch('CYN18,743,600,000.00');
})