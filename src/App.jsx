import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { Footer, Navbar } from './components/layout';
import { CurrencyTableContainer } from './components/currencyTable';
import { SingleCurrencyContainer } from './components/singleCurrency';
import { Settings } from './components/settings';

class App extends Component {
  render() {
    return (
      <Router>
        <MuiThemeProvider>
          <div>
            <nav>
              <Navbar />
            </nav>
            <main>
              <Route exact path='/' component={CurrencyTableContainer} />
              <Route path='/currency/:id' component={SingleCurrencyContainer} />
              <Route path='/settings' component={Settings} />
            </main>
            <Footer />
          </div>
        </MuiThemeProvider>
      </Router>
    );
  }
}

export default App;