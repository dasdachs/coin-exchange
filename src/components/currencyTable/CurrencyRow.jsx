import React from 'react';
import { Link } from 'react-router-dom';
import {
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';

import { formatCurrencyString } from '../../utils';

/**
 * CurrencyRow() is a presentational component function.
 * 
 * It returns a single TableRow with the data for a currency. The price is formated to local strings.
 */
const CurrencyRow = (props) => {
    const price = formatCurrencyString(props.price, props.userCurrency);
    const link = '/currency/' + props.currencyId;
    return(        
        <TableRow key={props.key} hoverable={true}>
            <TableRowColumn>{props.rank}</TableRowColumn>
            <TableRowColumn>{props.symbol}</TableRowColumn>
            <TableRowColumn>{price}</TableRowColumn>
            <TableRowColumn><span className={props.lastDay > 0 ? 'fa fa-arrow-up' : 'fa fa-arrow-down'}> {props.lastDay}</span></TableRowColumn>
            <TableRowColumn><Link to={link}>More</Link></TableRowColumn>
        </TableRow>
    )
}

export default CurrencyRow;