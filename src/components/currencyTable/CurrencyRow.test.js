import React from 'react';

import CurrencyRow from './CurrencyRow';

describe('Testing the CurrencyRow Component', () => {
    let wrapper;
    const currency = {
        "id": "bitcoin", 
        "name": "Bitcoin", 
        "symbol": "BTC", 
        "rank": "1", 
        "price_usd": "17114.6", 
        "price_btc": "1.0", 
        "24h_volume_usd": "18743600000.0", 
        "market_cap_usd": "287289731760", 
        "available_supply": "16786237.0", 
        "total_supply": "16786237.0", 
        "max_supply": "21000000.0", 
        "percent_change_1h": "-0.73", 
        "percent_change_24h": "0.72", 
        "percent_change_7d": "31.64", 
        "last_updated": "1515271460"
    };
    const userCurrency = 'EUR';

    beforeEach( () =>{
        wrapper = shallow(
            <CurrencyRow 
                rank={currency.rank}
                symbol={currency.symbol}
                price={currency.price_usd}
                lastDay={currency.percent_change_24h}
                currencyId={currency.id}
                userCurrency={userCurrency}
            />
        )
    })
    
    it('CurrencyRow renders succesfully', () => { 
        expect(wrapper).toMatchSnapshot();
    })
})“