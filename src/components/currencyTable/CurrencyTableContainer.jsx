import React, { Component } from 'react';
import { connect } from 'react-redux';

import { dataLoaded, getData, startLoading } from '../../actions';
import CurrencyTable from './CurrencyTable';
import { Loading } from '../layout';

class CurrencyTableContainer extends Component {
  componentDidMount() {
    if (this.props.data.currencies.length === 0) {
      this.props.startLoading();
      this.props.getData(this.props.data.userCurrency);
    }
    this.props.dataLoaded();
  }

  componentWillUnmount() {
    this.props.startLoading();
  }

  render() {
    return(
      <div className='currency-table'>
        { this.props.data.loading === false
            ? <CurrencyTable currencies={this.props.data.currencies} userCurrency={this.props.data.userCurrency} />
            : <Loading />
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    data: state
  }
}

const mapDispatchToProps = dispatch => (
  {
    dataLoaded: () => {dispatch(dataLoaded())},
    getData: fiat => {dispatch(getData(fiat))},
    startLoading: () => {dispatch(startLoading())}
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyTableContainer);