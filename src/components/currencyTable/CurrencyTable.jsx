import React from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow
} from 'material-ui/Table';

import { getJSONAttr } from '../../utils';
import CurrencyRow from './CurrencyRow';

import './CurrencyTable.css';

const CurrencyTable = (props) => {
  const currencyPriceInObject = getJSONAttr('price', props.userCurrency);
  return(
    <Table>
      <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
        <TableRow>
          <TableHeaderColumn>Rank</TableHeaderColumn>
          <TableHeaderColumn>Symbol</TableHeaderColumn>
          <TableHeaderColumn>Price {props.userCurrency}</TableHeaderColumn>
          <TableHeaderColumn>24h change</TableHeaderColumn>
          <TableHeaderColumn>Detail</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false} stripedRows={true} showRowHover={true}>
        { 
          props.currencies.map((currency) => (
              <CurrencyRow
                key={currency.id}
                rank={currency.rank}
                symbol={currency.symbol}
                price={currency[currencyPriceInObject]}
                lastDay={currency.percent_change_24h}
                currencyId={currency.id}
                userCurrency={props.userCurrency}
              />
          )
        )}
      </TableBody>
    </Table>
    
  )
}

export default CurrencyTable;