import React, { Component } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { setCurrency } from '../../actions';
import './Settings.css';

export class Settings extends Component {
    handleChange(event, index, value) {
        this.props.setCurrency(value);
    }
  
    render() {
        return(
            <div className='z-depth-2 settings'>
                <header>
                    <h1 className='center-align'>App Settings</h1>
                </header>
                <hr />
                <section className='row'>
                    <h5 className='center-align'>Select your preffered fiat currency</h5>
                    <div className='col s6 offset-s5'>
                        <SelectField
                            floatingLabelText="Select currency."
                            value={this.props.userCurrency}
                            onChange={this.handleChange.bind(this)}
                        >
                            <MenuItem value="USD" primaryText="USD" />
                            <MenuItem value="EUR" primaryText="EUR" />
                            <MenuItem value="CNY" primaryText="CNY" />
                        </SelectField>
                        <RaisedButton label="Go back" secondary={true} onClick={this.props.history.goBack.bind(this)} />
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        userCurrency: state.userCurrency
    }
  }
  
  const mapDispatchToProps = dispatch => (
    {
      setCurrency: newCurrency => {dispatch(setCurrency(newCurrency))}
    }
  )

export default connect(mapStateToProps, mapDispatchToProps)(Settings);