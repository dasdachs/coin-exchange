import React,  { Component } from 'react';
import { connect } from 'react-redux';

import { getData } from '../../actions';

import SingleCurrencyCard from './SingleCurrencyCard';
import { Loading } from '../layout';

class SingleCurrencyContainer extends Component {
    componentWillMount() {
        if (this.props.data.currencies.length === 0) {
            this.props.getData(this.props.data.userCurrency);
        }
    }
    
    render() {
        const currency = this.props.data.currencies.find( currency => {    
            return currency.id === this.props.match.params.id;
        });
        return(
            <div className='container'>
                { currency !== undefined
                    ? <SingleCurrencyCard currency={currency} userCurrency={this.props.data.userCurrency} /> 
                    : <Loading /> 
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      data: state
    }
}
  
const mapDispatchToProps = dispatch => ({
    getData: fiat => {dispatch(getData(fiat))}
})

export default connect(mapStateToProps, mapDispatchToProps)(SingleCurrencyContainer);