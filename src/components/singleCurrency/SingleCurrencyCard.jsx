import React from 'react';
import { Card, CardHeader, CardTitle } from 'material-ui/Card'
import { List, ListItem } from 'material-ui/List';

import { formatCurrencyString, getJSONAttr } from '../../utils';

import './SingleCurrencyCard.css';

const SingleCurrencyCard = (props) => {    
    const priceAttribue = getJSONAttr('price', props.userCurrency);
    const formatedPrice = formatCurrencyString(props.currency[priceAttribue], props.userCurrency);
    const volumeAttribue = getJSONAttr('24h_volume', props.userCurrency);
    const formatedVolume = formatCurrencyString(props.currency[volumeAttribue], props.userCurrency);
    const marketCapAttribue = getJSONAttr('market_cap', props.userCurrency);
    const formatedMarketCap = formatCurrencyString(props.currency[marketCapAttribue], props.userCurrency);

    return(
        <Card className='currency-card'>
            <CardHeader
                title={props.currency.symbol}
                subtitle={'Rank ' + props.currency.rank}
                
            />
            <div className='performance-section'>
                <p>
                    <span className={props.currency.percent_change_1h > 0 ? 'fa fa-chevron-up' : 'fa fa-chevron-down'}> {props.currency.percent_change_1h} (last hour) </span>
                    <span className={props.currency.percent_change_24h > 0 ? 'fa fa-chevron-up' : 'fa fa-chevron-down'}> {props.currency.percent_change_24h} (last day) </span>
                    <span className={props.currency.percent_change_7d > 0 ? 'fa fa-chevron-up' : 'fa fa-chevron-down'}> {props.currency.percent_change_7d} (last week) </span>
                </p>
            </div>
            <CardTitle 
                title={props.currency.name}
            />
            
            <List>
                <ListItem primaryText={'Price: ' + formatedPrice}   />
                <ListItem primaryText={'Price: BTC ' + props.currency.price_btc}   />
                <ListItem primaryText={'24h volume: ' + formatedVolume}  />
                <ListItem primaryText={'Market capitlisation: ' + formatedMarketCap}  />
                <br/>
                <ListItem primaryText={'Available supply: ' + props.currency.available_supply}  />
                <ListItem primaryText={'Total supply: ' + props.currency.total_supply}  />
            </List>
        </Card>
    )
}

export default SingleCurrencyCard;