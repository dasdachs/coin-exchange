import React from 'react';
import Footer from './Footer';

describe('Testing the Footer Component', () => {
    let wrapper;

    beforeEach( () =>{
        wrapper = shallow(<Footer />)
    })
    
    it('Renders Footer regardles of the year', () => { 
        const getFullYear = jest.fn(() => '2018');
        expect(wrapper).toMatchSnapshot();
    })
    
    it('Renders the current year in the Footer', () => {
        const date = new Date().getFullYear().toString();
        expect(wrapper.text()).toMatch(date);
    })
})