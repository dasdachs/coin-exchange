import React from 'react';
import { Link } from 'react-router-dom';
import AppBar from 'material-ui/AppBar';
import ActionSettings from 'material-ui/svg-icons/action/settings';

/**
 * Navbar() returns the AppBar with the title and a link to the settings menu
 */
const Navbar = () => {
  return(
    <AppBar
      title={<Link to='/'>Crypto Exchange</Link>}
      showMenuIconButton={false}
      iconElementRight={<Link to='/settings'><ActionSettings /></Link>}
    />
  )
}

export default Navbar;
