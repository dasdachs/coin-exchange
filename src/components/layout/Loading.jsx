import React from 'react';

import './Loading.css';

const Loading = () => {
    return(
        <div className="la-ball-climbing-dot">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    )
}

export default Loading;