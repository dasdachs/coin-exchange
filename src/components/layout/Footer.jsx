import React from 'react';

import './Footer.css';

const Footer = () => {
    return (
        <footer className='app-footer'>
            <p>&copy; Jani Šumak {(new Date().getFullYear())}</p>
            <div className='promo'>
                <p>
                    <a href='mailto:jani.sumak@gmail.com'><span className='fa fa-envelope-o'></span></a>
                    <a href='https://twitter.com/dasdachs'><span className='fa fa-twitter'></span></a>
                    <a href='https://github.com/dasdachs'><span className='fa fa-github'></span></a>
                </p>
            </div>
        </footer>
    )
}

export default Footer;