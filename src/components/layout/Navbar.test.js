import React from 'react';

import Navbar from './Navbar';

describe('Testing the Navbar Component', () => {    
    it('Renders succesfully', () => { 
        const wrapper = shallow(<Navbar />);
        expect(wrapper).toMatchSnapshot();
    })
})