import React from 'react';

import Loading from './Loading';

describe('Testing the Loading Component', () => {    
    it('Renders succesfully', () => { 
        const wrapper = shallow(<Loading />);
        expect(wrapper).toMatchSnapshot();
    })
})